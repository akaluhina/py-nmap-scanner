import os
import re

def find_online_hosts(file_path):
    pattern = '[0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}'
    hosts_up = []
    if not os.path.exists(file_path):
        print('No file')
        return False
    with open(file_path) as file:
        for line in file:
            ip = re.findall(pattern, line)
            if ip:
                if 'DOWN' in line:
                    continue
                hosts_up.append(ip[0])
    return hosts_up

found_hosts = find_online_hosts('nmap_ip_scanned_addresses.txt')
with open('found_hosts.txt', 'w') as file:
    for ip in found_hosts:
        file.write('Hosts ip: {0}\n'.format(ip))
